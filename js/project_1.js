const task = [
  {
    _id: "1djgkdje2546xjk",
    complated: true,
    body:
      "«Как JavaScript может быть асинхронным и однопоточным?» Если кратко, то JavaScript однопоточный, а асинхронное 				поведение не является частью самого языка; вместо этого оно построено на основе него в браузере (или среде 						программирования) и доступно через браузерные API. 1) Heap (куча) — объекты собраны в кучу, которая есть ни 					что иное, как название для наименее структурированной части памяти.  2) Stack (стопка, стек) — репрезентация единственного потока выполнения JavaScript-кода. Вызовы функций помещаются в стек (об этом ниже). 3) Browser or Web API’s (браузерные или веб API) — встроены в браузер и способны предоставлять данные из браузера и окружающей компьютерной среды и давать возможность выполнять с ними полезные и сложные вещи. Они не являются частью языка JavaScript, но они построены на его основе и предоставляют вам супер силы, которые можно использовать в JavaScript коде. Например Geolocation API предоставляет доступ к нескольким простым конструкциям JavaScript, которые используются для получения данных о местоположении, так что вы можете, скажем, отобразить своё местоположение на Google Map. В фоновом режиме браузер использует низкоуровневый код (например C++) для связи с оборудованием GPS устройства (или любым другим, доступным для определения данных о местоположении), получения данных о местоположении и возвращения их в среду браузера для использования в вашем коде. Но опять, эта сложность абстрагирована от вас посредством API. https://medium.com/devschacht/javascript-eventloop-explained-f2dcf84e36ee",
    title: "Объяснение работы EventLoop в JavaScript"
  },
  {
    _id: "2djgkdje2546xjk",
    complated: false,
    body: `The function statement declares a function.
		A declared function is “saved for later use”, and will be executed later, when it is invoked (called).
		Just as Variable Declarations must start with “var”, Function Declarations must begin with “function”.
		e.g.
		function bar() {
		return 3;
		}
		function is only declared here .For using it, it must be invoked using function name. e.g bar();
		What is a Function Expression?
		A JavaScript function can also be defined using an expression.
		A function expression can be stored in a variable:
		var x = function (a, b) {return a * b};
		After a function expression has been stored in a variable, the variable can be used as a function. Functions stored in variables do not need function names. They are always invoked (called) using the variable name.
		Function Expression VS. Function Statement
		Example: Function Expression
		alert(foo()); // ERROR! foo wasn't loaded yet
		var foo = function() { return 5; }
		Example: Function Declaration
		alert(foo()); // Alerts 5. Declarations are loaded before any code can run.
		function foo() { return 5; }
		Function declarations load before any code is executed while Function expressions load only when the interpreter reaches that line of code.
		Similar to the var statement, function declarations are hoisted to the top of other code. Function expressions aren’t hoisted, which allows them to retain a copy of the local variables from the scope where they were defined.
		Benefits of Function Expressions
		There are several different ways that function expressions become more useful than function declarations.
		As closures
		As arguments to other functions
		As Immediately Invoked Function Expressions (IIFE)
		Credits : Angus Croll , www.sitepoint.com`,
    title: "Function Declarations vs. Function Expressions"
  },
  {
    _id: "3djgkdje2546xjk",
    complated: true,
    body: `Встроенные типы
		С точки зрения JavaScript, тип — это набор характеристик значения, который отличает поведение одного типа значения от другого. Другими словами, если вы планируете использовать 42 как число, значит, тип данных для этого значения будет числом (number).
		JavaScript определяет 7 типов данных:
		null
		undefined
		boolean
		string
		number
		object
		symbol (добавлен в ES6)
		Все типы данных кроме объектов (objects) — примитивные. Оператор typeof() возвращает тип данных для значения в виде строки. Во всех случаях оператор typeof будет возвращать верное значение из списка кроме null. В случае с null оператор typeof вернет object. Это баг языка JavaScript. Несмотря на то, что при проверке typeof, null вернет “object”, при преобразовании в тип boolean null вернет false.
		Седьмой тип данных, который может вернуть typeof — “function”:
		typeof function a(){ /* .. */ } === "function"; // true
		Массивы также имеют тип данных object, потому что фактически массивы и есть объекты с той лишь разницей, что свойства массива являются нумерованными индексами.
		typeof [1,2,3] === “object”; // true
		Значения как типы данных
		В JavaScript у переменных нет типа — тип есть у значений. Переменные же могут содержать любое значение в любое время. В одном случае это может быть число (number), в другом — строка (string) и т.д..
		Undefined vs undeclared.
		Для переменных, которые были объявлены, но не были инициализированы тип данных вернет undefined.
		var a;
		typeof a; // "undefined"
		var b = 42;
		var c;
		// later
		b = c;
		typeof b; // "undefined"
		typeof c; // "undefined"
		В данном случае, переменная была объявлена, но на текущий момент, внутри переменной не хранится никакого значения. В случае, если переменная не была объявлена (undeclared) возникнет ошибка, однако при этом оператор typeof для undeclared переменной вернет undefined:
		var a;
		a; // undefined
		b; // ReferenceError: b is not defined
		typeof(b); //undefined
		Эта фича typeof является защитной т.к. в этом случае можно проверить переменную на undeclared без возникновения ReferenceError.
		Значения.
		Массивы.
		Если сравнивать со строго типизированными языками программирования в JavaScript массивы всего лишь контейнеры для любых типов значений: от строки(string), числа(number), до объекта(object), или даже другого массива (array):
		var a = [ 1, "2", [3] ];
		a.length; //3
		a[0] === 1; // true
		a[2][0] === 3; //true
		Нет необходимости заранее определять размер массива, можно лишь объявить его и добавлять элементы по мере необходимости.
		Будьте осторожны при создании массивов с пустыми ячейками.
		var a = [ ];
		a[0] = 1;
		//ячейка a[1] пуста
		a[2]=[ 3 ];
		a[1]; //undefined
		a.length //3
		Такой код может привести к странному поведению с пустыми ячейками, оставленными между элементами массива. Ячейка появляясь получает значение undefined, но ведет себя не совсем также как ячейка, которой undefined присвоено в явном виде.
		Ячейки массива имеют числовые индексы (как мы обычно и ожидаем), но хитрость в том что массивы также являются объектами и могут иметь свойство в виде строки ключ/значение, которое не будет считаться при подсчете размера массива в свойстве .length
		Как бы там ни было всегда есть риск попасться в ловушку, когда строковое значение, изначально предполагаемое как ключ будет сконвертировано в число:
		var a = [ ];
		a["13"] = 42;
		a.length; // 14
		Так что, для хранения ключ/свойство нужно использовать объекты, а для хранения значений в строго нумерованных ячейках — массивы.
		Массивоподобные.
		Различные операции по поиску элементов DOM возвращают список DOM элементов, которые являются массивоподобной коллекцией значений с нумерованными индексами. Поэтому, есть возможность конвертации массивоподобного значения в настоящий массив и применения методов массива (таких как indexOf(..), concat(..), forEach(..) и т.д.). Другой пример — когда функция представляет аргументы в виде массивоподобного объекта, для доступа к аргументам, как к списку значений.
		Строки.
		Есть распространенное мнение, что строки всего лишь массивы из символов. Важно понимать, что строки и массивы символов — не одно и то же. Строки имеют поверхностное сходство с массивами для них работают свойство .length, методы .indexOf() .concat().
		Однако, строки в JavaScript неизменяемы, тогда как массивы могут меняться. Для строки нет методов, которые меняют ее содержимое на месте, метод лучше создаст новую строку и вернет ее нам.
		Можно “одалживать” методы массивов которые не требуют изменения строки по месту либо конвертировать строку в массив каждый раз когда это понадобится.`,
    title: "Типы данных."
  },
  {
    _id: "4djgkdje2546xjk",
    complated: false,
    body: `Object.assign()
		Создаёт новый объект путём копирования значений всех собственных перечислимых свойств из одного или более исходных объектов в целевой объект.
		Object.create()
		Создаёт новый объект с указанными объектом прототипа и свойствами.
		Object.defineProperty()
		Добавляет к объекту именованное свойство, описываемое переданным дескриптором.
		Object.defineProperties()
		Добавляет к объекту именованные свойства, описываемые переданными дескрипторами.
		Object.freeze()
		Замораживает объект: другой код не сможет удалить или изменить никакое свойство.
		Object.getOwnPropertyDescriptor()
		Возвращает дескриптор свойства для именованного свойства объекта.
		Object.getOwnPropertyNames()
		Возвращает массив, содержащий имена всех переданных объекту собственных перечисляемых и неперечисляемых свойств.
		Object.getOwnPropertySymbols()
		Возвращает массив всех символьных свойств, найденных непосредственно в переданом объекте.
		Object.getPrototypeOf()
		Возвращает прототип указанного объекта.
		Object.is()
		Определяет, являются ли два значения различимыми (то есть, одинаковыми)
		Object.isExtensible()
		Определяет, разрешено ли расширение объекта.
		Object.isFrozen()
		Определяет, был ли объект заморожен.
		Object.isSealed()
		Определяет, является ли объект запечатанным (sealed).
		Object.keys()
		Возвращает массив, содержащий имена всех собственных перечислимых свойств переданного объекта.
		Object.observe()
		Асинхронно наблюдает за изменениями в объекте.
		Object.preventExtensions()
		Предотвращает любое расширение объекта.
		Object.seal()
		Предотвращает удаление свойств объекта другим кодом.
		Object.setPrototypeOf()
		Устанавливает прототип (т.е. внутреннее свойство [[Prototype]])`,
    title: "Методы конструктора Object"
  },
  {
    _id: "5djgkdje2546xjk",
    complated: false,
    body: `concat()	Merge two or more arrays, and returns a new array.
					copyWithin()	Copies part of an array to another location in the same array and returns it.
					entries()	Returns a key/value pair Array Iteration Object.
					every()	Checks if every element in an array pass a test in a testing function.
					fill()	Fill the elements in an array with a static value.
					filter()	Creates a new array with all elements that pass the test in a testing function.
					find()	Returns the value of the first element in an array that pass the test in a testing function.
					findIndex()	Returns the index of the first element in an array that pass the test in a testing function.
					forEach()	Calls a function once for each array element.
					from()	Creates an array from an object.
					includes()	Determines whether an array includes a certain element.
					indexOf()	Search the array for an element and returns its first index.
					isArray()	Determines whether the passed value is an array.
					join()	Joins all elements of an array into a string.
					keys()	Returns a Array Iteration Object, containing the keys of the original array.
					lastIndexOf()	Search the array for an element, starting at the end, and returns its last index.
					map()	Creates a new array with the results of calling a function for each array element.
					pop()	Removes the last element from an array, and returns that element.
					push()	Adds one or more elements to the end of an array, and returns the array's new length.
					reduce()	Reduce the values of an array to a single value (from left-to-right).
					reduceRight()	Reduce the values of an array to a single value (from right-to-left).
					reverse()	Reverses the order of the elements in an array.
					shift()	Removes the first element from an array, and returns that element.
					slice()	Selects a part of an array, and returns the new array.
					some()	Checks if any of the elements in an array passes the test in a testing function.
					sort()	Sorts the elements of an array.
					splice()	Adds/Removes elements from an array.
					toString()	Converts an array to a string, and returns the result.
					unshift()	Adds new elements to the beginning of an array, and returns the array's new length.
					values()	Returns a Array Iteration Object, containing the values of the original array.`,
    title: "The table lists the standard methods of the Array object."
  }
];

(function(arrOfTasks) {
  const objOfTasks = arrOfTasks.reduce((acc, task) => {
    acc[task._id] = task;
    // console.log(acc);
    // console.log(task);
    return acc;
  }, {});

  //Elements UI
  const listContent = document.querySelector(".tasks-list-section .list-group");
  const form = document.forms["addTask"];
  const inputTitle = form.elements["title"];
  const inputBody = form.elements["body"];

  //Events
  fragmentAlltask(objOfTasks);
  form.addEventListener("submit", onFormSubmitHendler);
  listContent.addEventListener("click", onDeleteFuncHendler);

  function fragmentAlltask(taskList) {
    if (!taskList) {
      alert("Pass the task list");
      return;
    }

    const fragment = document.createDocumentFragment();
    Object.values(taskList).forEach(task => {
      const li = listItemTamplate(task);
      fragment.appendChild(li);
      // console.log(li)
    });
    listContent.appendChild(fragment);
  }

  function listItemTamplate({ _id, title, body } = {}) {
    const li = document.createElement("li");
    li.classList.add(
      "list-group-item",
      "d-flex",
      "align-item-center",
      "flex-wrap",
      "mt-3"
    );
    li.setAttribute("data-task-id", _id);
    // console.log(li);

    const span = document.createElement("span");
    span.textContent = title;
    span.style.fontWeight = "bold";
    // console.log(span);

    const deleteBtn = document.createElement("button");
    deleteBtn.textContent = "Delete task";
    deleteBtn.classList.add("btn", "btn-danger", "ml-auto", "delete-btn");
    // console.log(deleteBtn)

    const article = document.createElement("p");
    article.textContent = body;
    article.classList.add("mt-2", "w-100");
    // console.log(article);

    li.appendChild(span);
    li.appendChild(deleteBtn);
    li.appendChild(article);
    // console.log(li)
    return li;
  }

  function onFormSubmitHendler(e) {
    e.preventDefault();
    const titleValue = inputTitle.value;
    const bodyValue = inputBody.value;

    if (!titleValue || !bodyValue) {
      alert("Please enter title or body");
      return;
    }

    const task = creatNewTask(titleValue, bodyValue); // newTask in => objOfTasks
    const listItem = listItemTamplate(task);
    listContent.insertAdjacentElement("afterbegin", listItem);
    form.reset();
    // console.log(objOfTasks);
  }

  function creatNewTask(title, body) {
    const newTask = {
      _id: `task-${Math.random()}`,
      complated: false,
      body,
      title
    };
    objOfTasks[newTask._id] = newTask;
    return { ...newTask }; // copy destructuring =>  ...newTask
  }

  function deleteTaskId(id) {
    const { title } = objOfTasks[id];
    const isConfirm = confirm(
      `It is you who want to delete the task ?: ${title}`
		);
		
		if(!isConfirm) return isConfirm;
		delete objOfTasks[id];
		return isConfirm;
	}
	
	function deleteTaskFormHtml(confirmed, el) {
		if(!confirmed) return;
		el.remove();
	}

  function onDeleteFuncHendler({ target }) {
    // console.log(e.target)
    if (target.classList.contains("delete-btn")) {
      const parent = target.closest("[data-task-id]");
			const id = parent.dataset.taskId;
			const confirmed = deleteTaskId(id);
			deleteTaskFormHtml(confirmed, parent);
    }
  }
})(task);
